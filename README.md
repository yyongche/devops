# README

## TOC

- Description
    - Addressing Scalability
- Tools For Kubernetes Cluster
- Kubernetes Cluster
    - Prerequisites
    - Setup
- Deployment On Kubernetes Cluster
    - Prerequisites
    - Namespace
    - Hardware Resources
        - Storage
    - Deployments
- Cleaning Up For Kubernetes Cluster
- REST API Endpoints
- Add A Message
    - Using `curl`

## Description

An user interacts with the stateless REST API in the `api` service by adding and retrieving messages. These messages are added and stored in the MongoDB database via the `api` service. The MongoDB database is created by the deployment of the `db` database.

### Addressing Scalability

For the `api` service, the [ReplicaSet](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/) is able to guarantee its availability. Instead of the [Deployments](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) used in this assignment, the [StatefulSets](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/) can be used to deploy the `db` database. The StatefulSets improves the overall resilience of the database, and together with [StorageClass](https://kubernetes.io/docs/concepts/storage/storage-classes/), [Persistent Volume](https://kubernetes.io/docs/concepts/storage/persistent-volumes/) and [Persistent Volume Claim](https://kubernetes.io/docs/concepts/storage/volumes/), additional storage can be added.

## Tools For Kubernetes Cluster

Assume that the client machine runs Ubuntu 18.04.

```
# Install Docker (Requires Restart After Installation)
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gp
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo usermod -aG docker $USER

# Install kubectl
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl

# Install k3d
curl -s https://raw.githubusercontent.com/rancher/k3d/main/install.sh | bash

# Install Helm
curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
sudo apt-get install apt-transport-https --yes
echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm

# Install Skaffold
curl -Lo skaffold https://storage.googleapis.com/skaffold/releases/latest/skaffold-linux-amd64 && sudo install skaffold /usr/local/bin/
```

## Kubernetes Cluster

### Prerequisites

1. [`kubectl`](https://kubernetes.io/docs/tasks/tools/) and [`k3d`](https://k3d.io/) are installed on the machine.

### Setup

2. Create a Kubernetes cluster and an image registry as follows.

```
k3d cluster create devops --registry-create devops-registry:0.0.0.0:5432
```

`k3d cluster create` also configures `kubectl` to access the above Kubernetes cluster.

## Deployment On Kubernetes Cluster

Take note that the commands in this section are to be executed at the root of this Git repository.

### Prerequisites

1. [`skaffold`](https://skaffold.dev/) and [`helm`](https://helm.sh/) are installed on the machine.
2. [`kubectl`](https://kubernetes.io/docs/tasks/tools/) is installed on the machine and configured to access the Kubernetes cluster.

### Namespace

3. Create namespace `devops-ns` as follows.

```
kubectl apply -f namespace.yaml
```

### Hardware Resources

#### Storage

4. Create storage class `devops-sc` as follows. Choose one of the storage classes to install.

```
# Storage Class: Local
git clone https://github.com/rancher/local-path-provisioner.git -b v0.0.21
helm install devops-sc ./local-path-provisioner/deploy/chart/ -n devops-ns --set storageClass.name=devops-sc
```

For other storage classes that are not listed above, take note to set the value for `storageClass.name` to `devops-sc`.

5. Create persistent volume and persistent volume claim as follows.

```
kubectl apply -n devops-ns -f db/volume.yaml
```

### Deployments

6. Create deployments as follows.

```
skaffold run -n devops-ns --default-repo devops-registry.localhost:5432
kubectl port-forward -n devops-ns service/api 8080:8080
```

`skaffold` builds and pushes the Docker image, then creates the deployments for the `api` service and the `db` database. `kubectl port-forward` allows the `api` service to be accessed via `http://localhost:8080`. `devops-registry.localhost:5432` can be substituted with another existing image registry.

## Cleaning Up For Kubernetes Cluster

Take note that the commands in this section are to be executed at the root of this Git repository.

```
skaffold delete -n devops-ns
kubectl delete -n devops-ns -f db/volume.yaml
helm delete devops-sc -n devops-ns
kubectl delete -f namespace.yaml
k3d cluster rm devops
```

## REST API Endpoints

- GET method on /messages[?limit=1000&offset=0&user=test]
    - Get messages.
    - `limit`, `offset` and `user` are optional and mutually exclusive.
    - Each message is provided with an URL. This URL retrieves its respective message content.
- GET method on /messages/{id}
    - Get a message with `id`.
- POST method on /messages
    - Add a message.
    - An example is shown below.

### Add A Message

#### Using `curl`

```
curl -X POST http://localhost:8080/messages -H 'Content-Type: application/json' -d '{"user":"user","content":"content"}'
```
