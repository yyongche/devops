package db

import (
	"testing"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

func TestCreateMessage(t *testing.T) {
	msg := Message{}
	msg.ID = primitive.NewObjectID()
	msg.CreatedAt = time.Now()
	msg.User = "user"
	msg.Content = "content"
	err := CreateMessage(msg)
	if err != nil {
		t.Errorf("%s", err.Error())
		return
	}
	t.Logf("%s", msg.ID.String())
	t.Logf("%s", msg.CreatedAt)
	t.Logf("%s", msg.User)
	t.Logf("%s", msg.Content)
}

func TestGetMessages(t *testing.T) {
	msgs, err := GetMessages(1000, 0)
	if err != nil {
		t.Errorf("%s", err.Error())
		return
	}
	for _, msg := range msgs {
		t.Logf("%s", msg.ID.String())
		t.Logf("%s", msg.CreatedAt)
		t.Logf("%s", msg.User)
	}
}
