package db

import (
	"context"
	"sync"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MessageHeader struct {
	ID        primitive.ObjectID `json:"-" bson:"_id"`
	CreatedAt time.Time          `json:"created_at" bson:"created_at"`
	User      string             `json:"user" bson:"user"`
	Url       string             `json:"url" bson:"-"`
}

type Message struct {
	ID        primitive.ObjectID `json:"-" bson:"_id"`
	CreatedAt time.Time          `json:"created_at" bson:"created_at"`
	User      string             `json:"user" bson:"user"`
	Content   string             `json:"content" bson:"content"`
}

var clientInstance *mongo.Client
var clientInstanceError error
var mongoOnce sync.Once

const (
	CONNECTION_URL  = "mongodb://mongo:27017"
	DATABASE_NAME   = "msgdb"
	COLLECTION_NAME = "col_msgs"
)

func GetMongoClient() (*mongo.Client, error) {
	mongoOnce.Do(func() {
		clientOptions := options.Client().ApplyURI(CONNECTION_URL)
		client, err := mongo.Connect(context.TODO(), clientOptions)
		if err != nil {
			clientInstanceError = err
		}
		err = client.Ping(context.TODO(), nil)
		if err != nil {
			clientInstanceError = err
		}
		clientInstance = client
	})
	return clientInstance, clientInstanceError
}

func CreateMessage(msg Message) error {
	client, err := GetMongoClient()
	if err != nil {
		return err
	}
	collection := client.Database(DATABASE_NAME).Collection(COLLECTION_NAME)
	_, err = collection.InsertOne(context.TODO(), msg)
	if err != nil {
		return err
	}
	return nil
}

func GetMessages(limit int64, offset int64, users ...string) ([]MessageHeader, error) {
	msgs := []MessageHeader{}
	client, err := GetMongoClient()
	if err != nil {
		return msgs, err
	}
	collection := client.Database(DATABASE_NAME).Collection(COLLECTION_NAME)

	filter := bson.M{}
	for _, user := range users {
		filter["user"] = user
		break
	}
	opts := options.Find()
	opts.SetSort(bson.M{"created_at": -1})
	opts.SetLimit(limit)
	opts.SetSkip(offset)

	cur, err := collection.Find(context.TODO(), filter, opts)
	if err != nil {
		return msgs, err
	}
	for cur.Next(context.TODO()) {
		msg := MessageHeader{}
		err = cur.Decode(&msg)
		if err != nil {
			return msgs, err
		}
		msgs = append(msgs, msg)
	}
	cur.Close(context.TODO())
	if len(msgs) == 0 {
		return msgs, mongo.ErrNoDocuments
	}
	return msgs, nil
}

func GetMessage(id string) (Message, error) {
	msg := Message{}
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return msg, err
	}

	client, err := GetMongoClient()
	if err != nil {
		return msg, err
	}
	collection := client.Database(DATABASE_NAME).Collection(COLLECTION_NAME)

	filter := bson.M{"_id": oid}
	err = collection.FindOne(context.TODO(), filter).Decode(&msg)
	if err != nil {
		return msg, err
	}
	return msg, nil
}
