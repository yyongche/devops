package main

import (
	"net/http"
	"strconv"
	"time"

	"github.com/aarnchng/messaging-api/db"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

const (
	DEFAULT_LIMIT  = 1000
	DEFAULT_OFFSET = 0
)

func main() {
	router := gin.Default()
	router.GET("/messages", getMessages)
	router.GET("/messages/:id", getMessage)
	router.POST("/messages", postMessage)
	router.Run("localhost:8080")
}

func postMessage(c *gin.Context) {
	msg := db.Message{}
	err := c.BindJSON(&msg)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}
	if len(msg.Content) >= 1000000 {
		c.JSON(http.StatusBadRequest, gin.H{"message": "message size > 1MB"})
		return
	}
	msg.ID = primitive.NewObjectID()
	msg.CreatedAt = time.Now()
	err = db.CreateMessage(msg)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}
	c.JSON(http.StatusCreated, msg)
}

func getMessages(c *gin.Context) {
	var limit int64
	var offset int64
	var err error

	limit, offset = DEFAULT_LIMIT, DEFAULT_OFFSET
	limitStr := c.Query("limit")
	if len(limitStr) > 0 {
		limit, err = strconv.ParseInt(limitStr, 10, 64)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}
		if limit <= 0 {
			limit = DEFAULT_LIMIT
		}
	}
	offsetStr := c.Query("offset")
	if len(offsetStr) > 0 {
		offset, err = strconv.ParseInt(offsetStr, 10, 64)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}
		if offset < 0 {
			offset = DEFAULT_OFFSET
		}
	}

	userStr := c.Query("user")
	urlForStr := c.Request.Host + c.Request.URL.Path + "/"
	if len(userStr) > 0 {
		msgs, err := db.GetMessages(limit, offset, userStr)
		if err != nil {
			c.JSON(http.StatusNotFound, gin.H{"message": err.Error()})
			return
		}
		for i := range msgs {
			msgs[i].Url = urlForStr + msgs[i].ID.Hex()
		}
		c.IndentedJSON(http.StatusOK, msgs)
	} else {
		msgs, err := db.GetMessages(limit, offset)
		if err != nil {
			c.JSON(http.StatusNotFound, gin.H{"message": err.Error()})
			return
		}
		for i := range msgs {
			msgs[i].Url = urlForStr + msgs[i].ID.Hex()
		}
		c.JSON(http.StatusOK, msgs)
	}
}

func getMessage(c *gin.Context) {
	id := c.Param("id")
	msg, err := db.GetMessage(id)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": err.Error()})
		return
	}
	c.JSON(http.StatusOK, msg)
}
